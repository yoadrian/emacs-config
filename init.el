;; no startup message
(setq inhibit-startup-message t)

;; default colors
(set-background-color "honeydew")
(set-foreground-color "black")
(set-cursor-color "black")

;; make the default colors show up by default!
(add-to-list 'default-frame-alist '(foreground-color . "black"))
(add-to-list 'default-frame-alist '(background-color . "honeydew"))
(add-to-list 'default-frame-alist '(cursor-color . "black"))

;; disable backup
(setq backup-inhibited t)

;; no noise
(setq ring-bell-function 'ignore)
 
;; show line and column numbers
(line-number-mode 1)
(column-number-mode 1)

;; Highlight parenthesis
(show-paren-mode 1)

;; default encoding
(prefer-coding-system 'utf-8)

;; word wrap
(global-visual-line-mode t)

;; use CUA mode for CTRL+x,c,v.
(cua-mode t)
    (setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
    (transient-mark-mode 1)               ;; No region when it is not highlighted
    (setq cua-keep-region-after-copy t) 
